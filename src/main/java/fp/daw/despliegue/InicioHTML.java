package fp.daw.despliegue;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class InicioHTML {

	static public void enviar(HttpServletResponse response, Connection connection, HttpSession session)
			throws IOException {
		PrintWriter out = null;

		try {
			response.setCharacterEncoding("utf-8");
			out = response.getWriter();
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<meta charset=\"UTF-8\" />");
			out.println("<title>Inicio de sesión</title>");
			out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/loginregistro.css\"");
			out.println(" media=\"screen\" />");
			out.println("<script type=\"text/javascript\" src=\"js/loginregistro.js\"></script>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>BIENVENIDO</h1>");
			out.println("</body>");
			out.println("</html>");
		} finally {
			if (out != null)
				out.close();
		}
	}

}
